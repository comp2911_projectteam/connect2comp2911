Hi all, this is a Connect 4 game developed by Adishwar Rishi, Aman Singh, Alan Allaf and Katrina Mok

The game is made in Java and the UI library used is Swing. The design of this application uses the MVC pattern.
However, the model is not persistent, a game cannot be saved. The model is created at startup, and destroyed at exit.

I hope anyone who tries this game has fun!
