package controller;

/**
 * The main controller interface
 * Defines all necessary actions the controller needs to be able to handle
 */
public interface Connect4Controller {
    //Getters and setters
    void setPlayer2Type (PlayerType p2);
    Difficulty getDifficulty();
    PlayerType getPlayer2Type();
    /**
     * Tells the controller to take action when the view has recieved an insert request
     * @param column the column the request was generated for
     */
    void handleInsertRequest(int column);

    /**
     * Allows the the caller to know which row would be used if a coin was dropped into that column
     * <strong>This method does NOT change the model</strong>
     * @param column
     * @return
     */
    int getInsertLocation (int column);

    /**
     * Tells the controller to take action when the view was notified about an mouse over
     * @param column the column the event was generated for
     */
    void handleMouseOverEvent(int column);

    /**
     * Tells the controller to take action when the view was notified about an mouse exit
     * @param column the column the event was generated for
     */
    void handleMouseExitEvent (int column);

    /**
     * Tells the controller to make preparations for a new game
     * @param d the difficulty of the new game
     * @param player2Type The type of player 2
     */
    void startNewGame (Difficulty d, PlayerType player2Type);

    /**
     * Contants for Player type
     */
    enum PlayerType {
        Human, AI
    }

    /**
     * Constants for the level of difficulty the game can handle
     */
    enum Difficulty {
        BEGINNER,EASY,MEDIUM,HARD
    }
}
