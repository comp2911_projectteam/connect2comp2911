package controller;

import AI.*;
import model.*;
import view.Connect4View;
import view.GameScreen;

import java.awt.*;

/**
 * The controller class handles all the logical decisions that need to be made to run the game<br>
 * It provides an interface between the View and the Model and handles the passing of information between the two
 * @author Adishwar Rishi, Allan Allaf
 */
public class Controller implements Connect4Controller, GameOverEventListener, TurnChangedEventListener {
    private Connect4Model model;
    private Connect4View view;
    private PlayerType player2Type;
    private Difficulty difficulty;
    private boolean gameOver;
    private GamePlayer gameAI;

    public Controller (Connect4Model model) {
        this.model = model;
        model.addGameOverEventListener(this);
        model.addTurnChangedEventListener(this);
        player2Type = PlayerType.AI;
        difficulty = Difficulty.EASY;
        gameAI = new Easy();
        view = new GameScreen("Connect 4",this,model);
        view.setVisible(true);
        gameOver = false;
    }

    //simple getters and setters
    @Override
    public void setPlayer2Type(PlayerType p2) {
        player2Type = p2;
    }
    @Override
    public Difficulty getDifficulty(){ return difficulty; }
    @Override
    public PlayerType getPlayer2Type(){ return player2Type; }

    @Override
    public void handleInsertRequest(int column) {
        if (!gameOver) {
            if (model.getInsertLocation(column) >= 0) {
                if (model.getCurrentPlayer() == Connect4Model.Player.PLAYER1) {
                    view.setCircleAt(column, model.getInsertLocation(column), Color.YELLOW);
                } else {
                    view.setCircleAt(column, model.getInsertLocation(column), Color.RED);
                }
                model.insetToColumn(column);
            }
        }
    }

    @Override
    public int getInsertLocation(int column) {
        return model.getInsertLocation(column);
    }

    @Override
    public void startNewGame(Difficulty d, PlayerType player2Type) {
        gameOver = false;
        model.reset();
        this.player2Type = player2Type;
        difficulty = d;
        if (player2Type == PlayerType.AI) {
            switch (difficulty) {
                case BEGINNER:
                    gameAI = new Beginner();
                    break;
                case EASY:
                    gameAI = new Easy();
                    break;
                case MEDIUM:
                    gameAI = new Medium();
                    break;
                case HARD:
                    gameAI = new Hard();
            }
        }
        view.reset();
    }

    @Override
    public void handleGameOverEvent(GameOverEvent e) {
        gameOver = true;
        view.displayGameOver(e.getWinner());
    }

    @Override
    public void handleTurnChangedEvent(TurnChangedEvent e) {
        view.turnOnPlayerLabel(e.getCurrentPlayer());
        if (player2Type == PlayerType.AI && e.getCurrentPlayer() == Connect4Model.Player.PLAYER2 && !gameOver) {
            int gameMove = gameAI.getNextMove(model.getGameState());
            view.setCircleAt(gameMove, model.getInsertLocation(gameMove), Color.RED);
            model.insetToColumn(gameMove);
        }
    }

    /**
     * The controller tells the view to show a faded circle at the location of the potential drop
     * @param column the column the event was generated for
     */
    @Override
    public void handleMouseOverEvent(int column){
        if (model.getInsertLocation(column) >= 0) {
            if (model.getCurrentPlayer() == Connect4Model.Player.PLAYER1) {
                view.setOpaqueCircleAt(column, model.getInsertLocation(column), new Color(220,220,128));
            } else {
                view.setOpaqueCircleAt(column, model.getInsertLocation(column), new Color(255,102,102));
            }
        }
    }

    /**
     * The controller removes the faded circle created by the handleMouseOverEvent
     * @param column the column the event was generated for
     */
    @Override
    public void handleMouseExitEvent (int column) {
        if (model.getInsertLocation(column) >= 0) {
            view.setOpaqueCircleAt(column, model.getInsertLocation(column), new Color(225,225,225,255));
        }
    }
}