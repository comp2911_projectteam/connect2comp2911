package model;

import java.util.EventListener;

/**
 * Any object that was to register for game over Events must implement this interface
 * @see GameState
 * @see GameOverEvent
 */
public interface GameOverEventListener extends EventListener {
    void handleGameOverEvent (GameOverEvent e);
}
