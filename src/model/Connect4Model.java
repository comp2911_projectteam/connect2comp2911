package model;

/**
 * Interface for possible actions and event listeners for the game state
 * Contains methods performing actions such as checking where pieces would land, and inserting a piece
 * @author Aman Singh, Adishwar Rishi
 */
public interface Connect4Model {

    /**
     * Method checks which row an insert into the column given would land in
     * @param column A number in the range 0-6 inclusive
     * @return Returns a number between 0-4 inclusive. Returns a negative number if entry into the column is illegal
     */
    int getInsertLocation (int column);

    /**
     * Method attempts to perform a move based on column given
     * Also checks if move made results in a win
     * @param column A number in the range 0-6 inclusive
     * @return player that made the insert, or INVALID_INSERT if attempted move is invalid
     */
    int insetToColumn (int column);

    /**
     * Adds the listener object to the internal list
     * @param listener a listener object
     */
    void addGameOverEventListener (GameOverEventListener listener);

    /**
     * Removes the listener object.<br>
     * Silently fails if the listener was never added
     * @param listener a listener object
     */
    public void removeGameOverEventListener (GameOverEventListener listener);

    /**
     * Adds the listener object to the internal list
     * @param listener a listener object
     */
    void addTurnChangedEventListener (TurnChangedEventListener listener);

    /**
     * Removes the listener object.<br>
     * Silently fails if the listener was never added
     * @param listener a listener object
     */
    public void removeTurnChangedEventListener (TurnChangedEventListener listener);

    /**
     * Resets the internal state of the model
     * This method should be called if a new game wants to be started
     */
    void reset ();

    /**
     * Passed state of the game board (usually to the AI)
     * @see GameBoard
     */
    GameBoard getGameState ();

    Player getCurrentPlayer ();

    /**
     * Enumerator for constant values representing players
     */
    enum Player {
        NO_ONE,PLAYER1,PLAYER2
    }
}
