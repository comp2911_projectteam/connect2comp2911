package model;

import java.util.ArrayList;

/**
 * The game state holds all information about the current game. It also contains the game engine (rules etc)
 * This game engine is what the controller has access to.
 * @author Aman Singh, Adishwar Rishi
 */
public class GameState implements Connect4Model {
    private GameBoard gameBoard;
    private ArrayList<GameOverEventListener> eventListeners;
    private ArrayList<TurnChangedEventListener> turnEventListeners;
    private Player currentPlayer;

    public GameState () {
        gameBoard = new GameBoard();
        eventListeners = new ArrayList<>();
        turnEventListeners = new ArrayList<>();
        currentPlayer = Player.PLAYER1;
    }

    /**
     * Method checks which row an insert into the column given would land in
     * @param column A number in the range 0-6 inclusive
     * @return Returns a number between 0-4 inclusive. Returns a negative number if entry into the column is illegal
     */
    public int getInsertLocation (int column) {
        return gameBoard.getInsertLocation(column);
    }

    /**
     * Method attempts to perform a move based on column given
     * Also checks if move made results in a win
     * @param column A number in the range 0-6 inclusive
     * @return player that made the insert, or INVALID_INSERT if attempted move is invalid
     */
    public int insetToColumn (int column) {
        int returnVal = gameBoard.insert(column,currentPlayer);
         if (returnVal != GameBoard.INVALID_INSERT) {
             int winResult = gameBoard.checkForWin(column,returnVal);
            if (winResult == GameBoard.GAME_WIN) {
                notifyAllGameOverListeners(currentPlayer);
            } else if (winResult == GameBoard.GAME_TIE) {
                notifyAllGameOverListeners(Player.NO_ONE);
            } else {
                if (currentPlayer == Player.PLAYER1) {
                    currentPlayer = Player.PLAYER2;
                } else if (currentPlayer == Player.PLAYER2) {
                    currentPlayer = Player.PLAYER1;
                }
                notifyAllTurnChangedListeners(currentPlayer);
            }
        }
        return returnVal;
    }

    /**
     * Adds the specified event listener to receive game over events from
     * this game state. Events occur when a player adds a something to a column in the game
     * and completes the game.
     * <strong>Assumes that listener will never be null</strong>
     * @param listener A NotNull object that will be called with the event
     */
    public void addGameOverEventListener (GameOverEventListener listener) {
        eventListeners.add(listener);
    }

    public void removeGameOverEventListener (GameOverEventListener listener) {
        eventListeners.remove(listener);
    }

    public void addTurnChangedEventListener (TurnChangedEventListener listener) {
        turnEventListeners.add(listener);
    }

    public void removeTurnChangedEventListener (TurnChangedEventListener listener) {
        turnEventListeners.remove(listener);
    }

    @Override
    public void reset() {
        gameBoard.reset();
        currentPlayer = Player.PLAYER1;
        notifyAllTurnChangedListeners(currentPlayer);
    }

    @Override
    public GameBoard getGameState() {
        try {
            return (GameBoard) gameBoard.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("CANT SEEM TO CLONE");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    private void notifyAllGameOverListeners (Player winner) {
        GameOverEvent e = new GameOverEvent(this,winner);
        for (GameOverEventListener listener : eventListeners) listener.handleGameOverEvent(e);
    }

    private void notifyAllTurnChangedListeners (Player currentPlayer) {
        TurnChangedEvent e = new TurnChangedEvent(this, currentPlayer);
        for (TurnChangedEventListener listener : turnEventListeners) listener.handleTurnChangedEvent(e);
    }
}
