package model;

import java.util.EventObject;

/**
 * This event is created every time a turn is changed in the model
 * @author Allan Allaf
 */
public class TurnChangedEvent extends EventObject{
    private Connect4Model.Player currentPlayer;

    public TurnChangedEvent(Object source, Connect4Model.Player currentPlayer){
        super(source);
        this.currentPlayer = currentPlayer;
    }

    public Connect4Model.Player getCurrentPlayer(){
        return this.currentPlayer;
    }
}
