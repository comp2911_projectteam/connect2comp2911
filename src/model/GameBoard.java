package model;

/**
 * Class which represents the current state of the Connect 4 board
 * Contains methods for actions such as adding pieces to the board,
 *      checking validity of moves and checking win conditions
 * @author Aman Singh, Adishwar Rishi, Katrina Mok
 */
public class GameBoard implements Cloneable {
    private GameState.Player[][] gameBoard;
    public static final int INVALID_INSERT = -1;
    public static final int INVALID_REMOVE = -1;
    public static final int GAME_WIN = 1;
    public static final int GAME_TIE = 2;
    public static final int GAME_NO_WIN = -2;

    public GameBoard () {
        gameBoard = new GameState.Player[7][6];
        for (int i = 0; i < gameBoard.length; i++) {
            for (int j = 0; j < gameBoard[i].length; j++) {
                gameBoard[i][j] = Connect4Model.Player.NO_ONE;
            }
        }
    }

    /**
     * Given a column, get the position of the topmost piece in the column
     * @param column A number in the range 0-6 inclusive
     * @return row position of topmost piece, or INVALID_REMOVE if there are no pieces
     */
    public int getRemoveLocation(int column) {
        for (int i = (gameBoard[column].length-1); i >= 0; i--) {
            if (gameBoard[column][i] != Connect4Model.Player.NO_ONE) {
                return i;
            }
        }
        return INVALID_REMOVE; 
    }

    /**
     * Given a column on the board and a player, find the topmost piece of that player's colour and remove it
     * @param column A number in the range 0-6 inclusive
     * @param player The current player
     * @return row position of removed piece, or INVALID_REMOVE if there were no pieces
     */
    public int remove(int column, Connect4Model.Player player) {
        for (int i = (gameBoard[column].length-1); i >= 0; i--) {
            if (gameBoard[column][i] == player) {
                gameBoard[column][i] = Connect4Model.Player.NO_ONE;
                return i;
            }
        }
        return INVALID_REMOVE;
    }

    /**
     * Given a column on the board, find the next possible place a piece could be placed in that column
     * Essentially finds the bottom-most empty space in the column
     * @param column A number in the range 0-6 inclusive
     * @return row position of bottom-most empty space, or INVALID_INSERT if column is full
     */
    public int getInsertLocation (int column) {
        for (int i = 0; i < gameBoard[column].length; i++) {
            if (gameBoard[column][i] == Connect4Model.Player.NO_ONE) {
                return i;
            }
        }
        return INVALID_INSERT;
    }

    /**
     * Given a column on the board and a player, attempt to insert the player's piece into the column
     * @param column A number in the range 0-6 inclusive
     * @param player The current player
     * @return row position of placed piece, or INVALID_INSERT if column was full and no insert was made
     */
    public int insert (int column, Connect4Model.Player player) {
        for (int i = 0; i < gameBoard[column].length; i++) {
            if (gameBoard[column][i] == Connect4Model.Player.NO_ONE) {
                gameBoard[column][i] = player;
                return i;
            }
        }
        return INVALID_INSERT;
    }

    /**
     * Given a row and a column in the board, find and return the state of the place
     * Essentially finds what player is in the space
     * @param row integer between 0 and 5
     * @param column integer between 0 and 6
     * @return PLAYER1, PLAYER2 or NO_ONE
     */
    public GameState.Player getPlayerAt (int row, int column) {
        return gameBoard[column][row];
    }

    /**
     * Given a position on the board, check if there is a piece there which is part of a winning combination
     * Essentially checks the piece in the place, then checks the pieces around it, then checks if there are
     *      four of the same in the row
     * @param col A number in the range 0-6 inclusive
     * @param row A number in the range 0-6 inclusive
     * @return GAME_WIN if there was a winning combination found, or GAME_NO_WIN if no combination was found
     */
    public int checkForWin (int col, int row) {
        GameState.Player playerThere = gameBoard[col][row];
        int tempr = row;
        int tempc = col;
        int pieceCount = 0;

        //go down and check vertically up
        while ((tempr != 0) && (gameBoard[tempc][tempr - 1] == playerThere)) {
            tempr--;
        }
        while (tempr < gameBoard[col].length) {
            if (gameBoard[tempc][tempr] == playerThere) { // if the piece is the same colour
                // prepare to check next piece
                pieceCount++;
                tempr++;
            } else break;
            if (pieceCount == 4) { // if saw 4 pieces of same player in column
                // we have a winner!!!
                return GAME_WIN;
            }
        }
//        go left and check to the right
        tempr = row;
        tempc = col;
        pieceCount = 0;
        while ((tempc != 0) && (gameBoard[tempc-1][tempr] == playerThere)) {
            tempc--;
        }
        while (tempc < gameBoard.length) {
            if (gameBoard[tempc][tempr] == playerThere) { // if the piece is the same colour
                // prepare to check next piece
                pieceCount++;
                tempc++;
            } else break;
            if (pieceCount == 4) { // if saw 4 pieces of same player in column
                // we have a winner!!!
                return GAME_WIN;
            }
        }
        //go diagonally down-left and check to the top
        tempr = row;
        tempc = col;
        pieceCount = 0;
        while (tempc != 0 && tempr != 0 && gameBoard[tempc-1][tempr-1] == playerThere) {
            tempc--;
            tempr--;
        }
        while (tempc < gameBoard.length && tempr < gameBoard[tempc].length) {
            if (gameBoard[tempc][tempr] == playerThere) {
                pieceCount++;
                tempc++;
                tempr++;
            } else break;
            if (pieceCount == 4) {
                return GAME_WIN;
            }
        }

        //go diagonally up-left and check to the top
        tempr = row;
        tempc = col;
        pieceCount = 0;
        while (tempc != 0 && tempr != gameBoard[tempc].length-1 && gameBoard[tempc-1][tempr+1] == playerThere) {
            tempc--;
            tempr++;
        }
        while (tempc < gameBoard.length && tempr >= 0) {
            if (gameBoard[tempc][tempr] == playerThere) {
                pieceCount++;
                tempc++;
                tempr--;
            } else break;
            if (pieceCount == 4) {
                return GAME_WIN;
            }
        }
        if (isTied()) {
            return GAME_TIE;
        } else {
            return GAME_NO_WIN;
        }
    }

    /**
     * Completely clears the board of all pieces
     */
    public void reset() {
        for (int i = 0; i < gameBoard.length; i++) {
            for (int j = 0; j < gameBoard[i].length; j++) {
                gameBoard[i][j] = Connect4Model.Player.NO_ONE;
            }
        }
    }

    /**
     * Checks if the game has been tied
     * Essentially checks if the entire board has been filled with pieces, then the game has been tied
     * @return true if the game has been tied, false if the game has not been tied
     */
    private boolean isTied () {
        boolean tied = true;
        for (int i = 0; i < gameBoard.length; i++) {
            tied = tied & (gameBoard[i][gameBoard[i].length-1] != Connect4Model.Player.NO_ONE);
        }
        return tied;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
