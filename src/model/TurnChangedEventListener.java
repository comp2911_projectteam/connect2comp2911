package model;

import java.util.EventListener;

/**
 * Any object that wants to register for a turn changed event must implement the interface
 * @author Alan Allaf
 */
public interface TurnChangedEventListener extends EventListener {
    void handleTurnChangedEvent (TurnChangedEvent e);
}
