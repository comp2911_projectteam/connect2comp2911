package view;

/**
 * @author Alan Allaf
 */

import controller.Connect4Controller;
import model.Connect4Model;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class represents the main screen for the game
 * The screen contains the game board, and is the main point of interaction with the user
 * @author Adishwar Rishi, Allan Allaf
 */
public class GameScreen extends JFrame implements Connect4View {
    private Connect4Controller controller;
    GamePanel gamePanel;
    TopGamePanel topPanel;
    GridComponent lastMoveCell;
    //the menu items
    JMenuItem beginnerGame;
    JMenuItem easyGame;
    JMenuItem mediumGame;
    JMenuItem hardGame;
    JMenuItem multiplayer;
    java.util.Timer gameTimerThread;

    public GameScreen(String title, Connect4Controller controller, Connect4Model model) {
        super(title);
        //setting up MVC
        this.controller = controller;
        setSize(800, 600);
        setResizable(false);

        //now setting up menu bar + initializing all inner items
        JMenuBar menuBar = new JMenuBar();
        JMenu gameMenu = new JMenu("New Game");
        beginnerGame = new JMenuItem("Beginner");
        beginnerGame.addActionListener(new MenuItemListener());
        easyGame = new JMenuItem("Easy");
        easyGame.addActionListener(new MenuItemListener());
        mediumGame = new JMenuItem("Medium");
        mediumGame.addActionListener(new MenuItemListener());
        hardGame = new JMenuItem("Hard");
        hardGame.addActionListener(new MenuItemListener());
        multiplayer = new JMenuItem("Multiplayer");
        multiplayer.addActionListener(new MenuItemListener());

        //adding a reset button to the top bar
        JMenu resetOption = new JMenu("Reset");
        final Connect4Controller con = this.controller;
        resetOption.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
                con.startNewGame(con.getDifficulty(), con.getPlayer2Type());
            }
            @Override
            public void menuDeselected(MenuEvent e) {
            }
            @Override
            public void menuCanceled(MenuEvent e) {
            }
        });

        //setting up the bar
        gameMenu.add(beginnerGame);
        gameMenu.add(easyGame);
        gameMenu.add(mediumGame);
        gameMenu.add(hardGame);
        gameMenu.add(multiplayer);
        menuBar.add(gameMenu);
        menuBar.add(resetOption);

        //adding menu bar to the frame
        this.setJMenuBar(menuBar);

        //setting up center component
        this.gamePanel = new GamePanel(7,6);
        Color c = new Color(255,255,255,255);
        gamePanel.setBorder(BorderFactory.createMatteBorder(40, 40, 40, 40, c));
        //adding center component to the frame
        this.add(gamePanel, BorderLayout.CENTER);

        //setting up north component
        TopGamePanel topGamePanel = new TopGamePanel();
        this.topPanel = topGamePanel;
        this.topPanel.setBackground(c);

        //setting up window exit listener
        WindowListener exitListener = new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                gameTimerThread = new Timer();
                gameTimerThread.scheduleAtFixedRate(new GameTimer(), 0, 1 * 1000);
            }

            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                gameTimerThread.cancel();
            }
        };
        this.addWindowListener(exitListener);

        //adding top component to frame
        this.add(topGamePanel, BorderLayout.NORTH);

        //setting up frame options
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
    }

    public void displayGameOver (Connect4Model.Player winner) {
        String message = "";
        gameTimerThread.cancel();
        switch (winner) {
            case PLAYER1:
                message = "Player 1 wins";
                break;
            case PLAYER2:
                message = "Player 2 wins";
                break;
            case NO_ONE:
                message = "It's a tie!";
        }
        String Options[] = {"Restart", "Quit"};
        int playerChoice = JOptionPane.showOptionDialog(this, message, "Game Over", JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE, null, Options, "Reply");
        if(playerChoice == 0){
            controller.startNewGame(controller.getDifficulty(), controller.getPlayer2Type());
        } else if (playerChoice == 1) {
            System.exit(0);
        } else if (playerChoice == JOptionPane.CLOSED_OPTION) {
//            System.out.println("DIALOUGE WAS CLOSED");
        }
    }

    @Override
    public void turnOnPlayerLabel(Connect4Model.Player p) {
        switch (p){
            case PLAYER1:
                this.topPanel.player1Label.setBackground(Color.YELLOW);
                this.topPanel.player1Label.setOpaque(true);
                this.topPanel.player2Label.setBackground(Color.WHITE);
                break;
            case PLAYER2:
                this.topPanel.player2Label.setBackground(Color.RED);
                this.topPanel.player2Label.setOpaque(true);
                this.topPanel.player1Label.setBackground(Color.WHITE);
                break;
        }
    }

    @Override
    public String getCurrentPlayerName(){
        String diff = null;
        if(controller.getPlayer2Type() == Connect4Controller.PlayerType.AI ){
            if(controller.getDifficulty() == Connect4Controller.Difficulty.BEGINNER){
                diff = "Beginner AI";
            } else if (controller.getDifficulty() == Connect4Controller.Difficulty.EASY){
                diff = "Easy AI";
            } else if (controller.getDifficulty() == Connect4Controller.Difficulty.MEDIUM){
                diff = "Medium AI";
            } else if (controller.getDifficulty() == Connect4Controller.Difficulty.HARD){
                diff = "Hard AI";
            }
        } else {
            diff = "Player 2";
        }
        return diff;
    }

    @Override
    public void setCircleAt(int column, int row, Color c) {
        GridComponent insertLocation = gamePanel.getGameGrid()[column][row];
        insertLocation.setCellColor(c);
        insertLocation.setOuterCellColor(Color.WHITE);
        insertLocation.repaint();
        if (lastMoveCell != null) {
            lastMoveCell.setOuterCellColor(Color.BLACK);
            lastMoveCell.repaint();
        }
        lastMoveCell = insertLocation;
    }

    @Override
    public void setOpaqueCircleAt (int column, int row, Color c) {
        GridComponent insertLocation = gamePanel.getGameGrid()[column][row];
        insertLocation.setCellColor(c);
        insertLocation.repaint();
    }

    @Override
    public void reset() {
        for (int i = 0; i < gamePanel.getGameGrid().length;i++) {
            for (int j = 0; j < gamePanel.getGameGrid()[i].length; j++) {
                GridComponent insertLocation = gamePanel.getGameGrid()[i][j];
                insertLocation.setCellColor(new Color(225, 225, 225, 255));
                insertLocation.setOuterCellColor(Color.BLACK);
                insertLocation.repaint();
            }
        }
        gameTimerThread.cancel();
        topPanel.getTimerLabel().setText("00:00");
        gameTimerThread = new Timer();
        gameTimerThread.scheduleAtFixedRate(new GameTimer(), 1000, 1000);
        String name = getCurrentPlayerName();
        topPanel.player2Label.setText(name);

    }

    private class TopGamePanel extends JPanel {
        //this needs to have some sort of timer interface
        JLabel timerOut;
        JLabel player1Label;
        JLabel player2Label;

        public TopGamePanel () {
            //initializing components
            timerOut = new JLabel("00:00");
            timerOut.setFont(new Font("Rosario", Font.PLAIN, 18));
            timerOut.setHorizontalAlignment(SwingConstants.CENTER);

            player1Label = new JLabel("Player 1");
            player1Label.setFont(new Font("Arial", Font.BOLD, 20));
            player1Label.setHorizontalAlignment(SwingConstants.CENTER);
            player1Label.setBackground(Color.YELLOW);
            player1Label.setOpaque(true);

            String diff = getCurrentPlayerName();
            player2Label = new JLabel(diff);
            player2Label.setFont(new Font("Arial", Font.BOLD, 20));
            player2Label.setHorizontalAlignment(SwingConstants.CENTER);
            player1Label.setOpaque(true);
            //setting up the layout
            GridLayout gridLayout = new GridLayout(1,3);
            this.setLayout(gridLayout);
            this.add(player1Label);
            this.add(timerOut);
            this.add(player2Label);
        }

        public JLabel getTimerLabel () {
            return timerOut;
        }

    }

    private class GamePanel extends JPanel {
        private int width, height;
        private GridComponent[][] gameGrid;

        public GamePanel (int width, int height) {
            super();
            this.width = width;
            this.height = height;
            gameGrid = new GridComponent[width][height];
            //setting up mouse event handling
            MouseListener mouseListener = new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    super.mousePressed(e);
                    GridComponent source = (GridComponent) e.getSource();
                    int col = source.x;
                    controller.handleInsertRequest(col);
                }
                @Override
                public void mouseEntered(MouseEvent e) {
                    super.mouseEntered(e);
                    GridComponent source = (GridComponent) e.getSource();
                    int column = source.x;
                    controller.handleMouseOverEvent(column);
                }
                @Override
                public void mouseExited(MouseEvent e) {
                    super.mouseExited(e);
                    GridComponent source = (GridComponent) e.getSource();
                    int column = source.x;
                    controller.handleMouseExitEvent(column);
                }
            };

            //setting up layout
            GridLayout gridLayout = new GridLayout(this.height,this.width);
            this.setLayout(gridLayout);
            for (int h = height - 1; h >= 0; h--) {
                for (int w = 0; w < width; w++) {
                    gameGrid[w][h] = new GridComponent(w,h);
                    gameGrid[w][h].addMouseListener(mouseListener);
                    this.add(gameGrid[w][h]);
                }
            }

        }

        public GridComponent[][] getGameGrid () {
            return gameGrid;
        }
    }

    private class GridComponent extends JPanel {
        public final int x;
        public final int y;
        private Color cellColor;
        private Color outerCellColor;

        public GridComponent (int x, int y) {
            this.x = x;
            this.y = y;
            Color c = new Color(0,120,240,255);
            this.setBackground(c);
            this.setPreferredSize(new Dimension(100, 100));  // try commenting out this line!

            cellColor = new Color(225,225,225,255);
            outerCellColor = Color.BLACK;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(outerCellColor);
            Graphics2D g2d = (Graphics2D) g.create();
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.fillOval(5, 5, 90, 90);
            g2d.dispose();
            g.setColor(cellColor);
            Graphics2D g2dd = (Graphics2D) g.create();
            g2dd.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2dd.fillOval(10, 10, 80, 80);
            g2dd.dispose();
        }

        public void setCellColor (Color c) {
            cellColor = c;
        }

        public void setOuterCellColor (Color c) {
            outerCellColor = c;
        }
    }

    private class MenuItemListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            if(e.getSource() == beginnerGame){
                controller.startNewGame(Connect4Controller.Difficulty.BEGINNER, Connect4Controller.PlayerType.AI);
            } else if (e.getSource() == easyGame) {
                controller.startNewGame(Connect4Controller.Difficulty.EASY, Connect4Controller.PlayerType.AI);
            } else if (e.getSource() == mediumGame) {
                controller.startNewGame(Connect4Controller.Difficulty.MEDIUM, Connect4Controller.PlayerType.AI);
            } else if (e.getSource() == hardGame) {
                controller.startNewGame(Connect4Controller.Difficulty.HARD, Connect4Controller.PlayerType.AI);
            } else if (e.getSource() == multiplayer) {
                //the difficulty does not matter
                controller.startNewGame(Connect4Controller.Difficulty.EASY, Connect4Controller.PlayerType.Human);
            }
        }
    }

    private class GameTimer extends TimerTask {
        private int seconds;

        public GameTimer () {
            seconds = 0;
        }

        @Override
        public void run() {
            seconds++;
            topPanel.getTimerLabel().setText(returnTimerDisplay(seconds));
        }

        private String returnTimerDisplay (int seconds) {
            if (seconds >= 60) {
                int min = seconds / 60;
                int sec = seconds - (60 * min);
                return ((min < 10)? "0"+min : ""+min) + ":" + ((sec < 10)? "0"+sec : ""+sec);
            } else {
                return "00:" + ((seconds < 10)? "0"+seconds : "" + seconds);
            }
        }
    }
}