package view;

import model.Connect4Model;

import java.awt.*;

/**
 * The view interface
 */
public interface Connect4View  {
    /**
     * Sets the circle with color c at the coordinates given
     * This method needs to be called when position has been accepted by the model
     * @param column the column
     * @param row the row
     * @param c The color to display
     */

    void setCircleAt (int column, int row, Color c);

    /**
     * Sets the circle with color c at the coordinates given
     * This method needs to be called before the position has been accepted by the model
     * @param column the column
     * @param row the row
     * @param c The color to display
     */
    public void setOpaqueCircleAt (int column, int row, Color c);

    /**
     * Highlights the label for the Player p
     * @param p a player defined in Connect4Model
     */
    public void turnOnPlayerLabel(Connect4Model.Player p);

    /**
     * Displays the game over dialouge
     * @param winner the winning player
     */
    public void displayGameOver (Connect4Model.Player winner);

    /**
     * Makes the view visible
     * @param b a boolean
     */
    void setVisible (boolean b);

    /**
     * Gets the current player according to the view
     * @return
     */
    String getCurrentPlayerName();

    /**
     * Resets the view
     * This method should be called when a new game is started
     */
    void reset ();
}
