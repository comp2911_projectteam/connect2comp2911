import controller.Connect4Controller;
import controller.Controller;
import model.Connect4Model;
import model.GameState;

/**
 * This is the main class that launches the game
 * The model is created here.
 * @author Everyone
 */
public class Connect4 {
    public static void main (String[] args) {
        Connect4Model model = new GameState();
        Connect4Controller connect4Controller = new Controller(model);
    }
}
