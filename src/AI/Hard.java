package AI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import model.Connect4Model;
import model.GameBoard;

/**
 * Class that represents an AI with hard difficulty
 * This AI's strategy involves using a min and max algorithm as well as alpha and beta trimming to determine the best move
 * The evaluation heuristic determines the best column for the AI by looking steps ahead.
 * @author Katrina Mok
 */
public class Hard implements GamePlayer {
    
    private static final int NUM_ROWS = 6;
    private static final int NUM_COLS = 7;
    private static final int MAX_DEPTH = 7;
    
    /**
     * Given the current state of board, it returns the next move made by the AI
     * @param board the current state of the board
     * @return column a valid column number from 0 to 6
     */
    public int getNextMove(GameBoard board) {
        int[] move = minmax(MAX_DEPTH, Connect4Model.Player.PLAYER2, board, 0, 0, 
                    Integer.MIN_VALUE, Integer.MAX_VALUE);
        return move[1];
    }
    
    /**
     * Given the depth, the player, and the board it determines the best move for the AI recursively
     * The other parameters of prevRow, prevCol, alpha and beta are used during recursion to find best column
     * The AI is trying to minimize the maximum possible loss made by the player
     * @param depth  the depth is essentially the restriction of the number of min-max recursions and how many steps to look ahead
     * @param p the player currently either PLAYER_1 or PLAYER_2
     * @param board the game board state
     * @param prevRow the previous moves row location
     * @param prevCol the previous moves column location
     * @param alpha the maximum value found for max board state
     * @param beta the minimum value found for min board state
     * @return an array containing the best rating and it's respective column
     */
    private int[] minmax(int depth, Connect4Model.Player p, GameBoard board, int prevRow, int prevCol, 
                    int alpha, int beta) {
        
        ArrayList<Integer> nextMoves = possibleColumns(board);
        long seed = System.nanoTime();
        Collections.shuffle(nextMoves, new Random(seed));
        int bestColumn = -1;
        int currRating = 0;
        int bestRating = 0;
        
        if (board.getPlayerAt(prevRow, prevCol) == Connect4Model.Player.NO_ONE)  {
            if (nextMoves.isEmpty() || depth == 0) {
                currRating = overallRating(board, Connect4Model.Player.NO_ONE);
                bestRating = -1;
                //return new int[] {currRating, bestColumn};
                return new int[] {currRating, prevCol};
            } 
        } else {        
            if (nextMoves.isEmpty() || depth == 0
                    || board.checkForWin(prevCol, prevRow) == GameBoard.GAME_WIN
                    || board.checkForWin(prevCol, prevRow) == GameBoard.GAME_TIE) {
                if (board.checkForWin(prevCol, prevRow) == GameBoard.GAME_WIN) {
                    if (p == Connect4Model.Player.PLAYER2) {
                        currRating = overallRating(board, Connect4Model.Player.PLAYER1);
                    } else {
                        currRating = overallRating(board, Connect4Model.Player.PLAYER2);
                    }
                } else {
                    currRating = overallRating(board, Connect4Model.Player.NO_ONE);
                }
                bestRating = -1;
                //return new int[] {currRating, bestColumn};
                return new int[] {currRating, prevCol};
            } 
        }
        
        if (bestRating == 0) {
                for (int colE : nextMoves) {
                    int rowInsert = board.insert(colE, p);
                        if (p == Connect4Model.Player.PLAYER1) {
                            currRating = minmax(depth - 1, Connect4Model.Player.PLAYER2, board, rowInsert, colE, 
                                    alpha, beta)[0];
                            if (currRating < beta) {
                                beta = currRating;
                                bestColumn = colE;
                            } else if (currRating == beta) {
                                Random rand = new Random();
                                int answer = rand.nextInt(2);
                                if (answer == 0) {
                                    beta = currRating;
                                    bestColumn = colE;
                                }    
                            }
                        } else { // PLAYER2)
                            currRating = minmax(depth - 1, Connect4Model.Player.PLAYER1, board, rowInsert, colE, 
                                    alpha, beta)[0];
                            if (currRating > alpha) {
                                alpha = currRating;
                                bestColumn = colE;
                            } 
                        }
                    board.remove(colE, p);
                    if (alpha >= beta)  {
                        break;
                    }
                }
            if (p == Connect4Model.Player.PLAYER2) {
                currRating = alpha;
            } else {
                currRating = beta;
            }
        }
        
        return new int[] {currRating, bestColumn};
    }
    /**
     * Given the state of the board, it returns all the columns which are not full
     * @param board the current state of the board
     * @return a list of valid column numbers 
     */
    private ArrayList<Integer> possibleColumns(GameBoard board) {
        ArrayList<Integer> retList = new ArrayList<>();

        for (int i = 0; i < NUM_COLS; i++) {
            if (board.getInsertLocation(i) != GameBoard.INVALID_INSERT) {
                retList.add(i);
            }
        }
        return retList;
    }
    
    /**
     * Given a board and the player, it rates the current board overall in terms of what it contains
     * The overall rating is dependent on horizontal, vertical, diagonal left and right
     * @param board the game board state
     * @param p the player currently either PLAYER_1,PLAYER_2, or NO_ONE
     * @return the overall rating of the current board state
     */
    private int overallRating(GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        
        int row = 0;
        int col = 0;
        Connect4Model.Player curr;
        
        if (p == Connect4Model.Player.NO_ONE || p == Connect4Model.Player.PLAYER2) {
            curr = p;
        } else {
            curr = Connect4Model.Player.PLAYER1;
        }
        
        while (row < NUM_ROWS) {
            rating += horizontalRating(row, board, curr);
            rating += 10*(NUM_ROWS-row);
             // AI wants to get best rating.
            row++;
        }
        
        row = 0;
        while (col < NUM_COLS) {
            //rating += verticalRating(col, board, curr);
            int temp = verticalRating(col, board, curr);
            if (col == 3) {
                rating += temp*2;
            } else if (col == 2 || col == 4) {
                rating += temp*3;
            } else if (col == 1 || col == 5) {
                rating += temp*4;
            } else {
                rating += temp*5;
            }
            col++;
        }
        col = 0;
                    
        // [3, 0], [2, 0], [1, 0], [0, 0], [0, 1], [0, 2]
        // [3,1], [2,1], [3,2], [1, 1], [2, 2], [1,2] 
        // diagonal right list
        rating += diagonalRightRating(3, 0, board, curr)*3; // row 0
        rating += diagonalRightRating(2, 0, board, curr)*3; // row 0
        rating += diagonalRightRating(1, 0, board, curr)*3; // row 0
        rating += diagonalRightRating(0, 0, board, curr)*3; // row 0
        
        rating += diagonalRightRating(0, 1, board, curr)*2; // row 1
        rating += diagonalRightRating(3, 1, board, curr)*2; // row 1
        rating += diagonalRightRating(2, 1, board, curr)*2; // row 1
        rating += diagonalRightRating(1, 1, board, curr)*2; // row 1
        
        rating += diagonalRightRating(0, 2, board, curr); // row 2
        rating += diagonalRightRating(3, 2, board, curr); // row 2
        rating += diagonalRightRating(2, 2, board, curr); // row 2
        rating += diagonalRightRating(1, 2, board, curr); // row 2
        //[3, 0], [4, 0], [5, 0], [6, 0], [6, 1], [6, 2]
        // [3,1], [4, 1], [3,2], [5,1], [4, 2], [5, 2]
        // diagonal left list
        rating += diagonalLeftRating(3, 0, board, curr)*3; // row 0
        rating += diagonalLeftRating(4, 0, board, curr)*3; // row 0
        rating += diagonalLeftRating(5, 0, board, curr)*3; // row 0
        rating += diagonalLeftRating(6, 0, board, curr)*3; // row 0
        
        rating += diagonalLeftRating(6, 1, board, curr)*2; // row 1
        rating += diagonalLeftRating(3, 1, board, curr)*2; // row 1
        rating += diagonalLeftRating(4, 1, board, curr)*2; // row 1
        rating += diagonalLeftRating(5, 1, board, curr)*2; // row 1
        
        rating += diagonalLeftRating(6, 2, board, curr); // row 2
        rating += diagonalLeftRating(3, 2, board, curr); // row 2
        rating += diagonalLeftRating(4, 2, board, curr); // row 2
        rating += diagonalLeftRating(5, 2, board, curr); // row 2
        
        
        if (curr == Connect4Model.Player.PLAYER1) {
            rating = rating*2 - 100000;
        } 
        if (curr == Connect4Model.Player.PLAYER2) {
            rating = rating*2 + 100000;
        }

        return rating;
    }
    
     /**
     * Given the row, the board and the player, returns the horizontal rating across a row
     * A positive rating is given if the number in a row is mostly p
     * A negative rating is given if the number in a row is mostly not p
     * @param row the row required to be rated
     * @param board the current state of the board
     * @param p the player to rate by
     * @return returns a positive or negative integer rating of a row depending if it on the player p
     */
    private int horizontalRating(int row, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int c = 0;
        int r = row;
        int inARowOne = 0;
        int inARowTwo = 0;
            // check along the whole horizontal
            while (c < NUM_COLS) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { 
                    inARowTwo = 0;
                    rating = ratingScale(rating, inARowOne, p, check);
                    inARowOne++;
                } else if (check == Connect4Model.Player.NO_ONE) {//empty cell
                    if (inARowOne == 0) {
                        rating--;
                    } else if (inARowTwo == 0) {
                        rating++;   
                    }
                } else { // other player D:!
                    inARowOne = 0;
                    if (check == Connect4Model.Player.PLAYER1) {
                        rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER1);
                    } else {
                        rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER2);
                    }
                inARowTwo++;
                }
                c++;
            }

        
        return rating; 
    } 
    
     /**
     * Given the column, the board and the player, returns the vertical rating from the col going up
     * A positive rating is given if the number in a row is mostly p
     * A negative rating is given if the number in a row is mostly not p
     * @param col the column required to be rated
     * @param board the current state of the board
     * @param p the player to rate by
     * @return returns a positive or negative integer rating depending on the player p
     */
    private int verticalRating(int col, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int r = 0;
        int c = col;
        int inARowOne = 0;
        int inARowTwo = 0;
        
        while (r < NUM_ROWS) {
            Connect4Model.Player check = board.getPlayerAt(r, c);
            if (check == p) { 
                inARowTwo = 0;
                rating = ratingScale(rating, inARowOne, p, check);
                inARowOne++;
            } else if (check == Connect4Model.Player.NO_ONE) {//empty cell
                    if (inARowOne == 0) {
                        rating--;
                    } else if (inARowTwo == 0) {
                        rating++;   
                    }
            } else { // other player D:!
                inARowOne = 0;
                if (check == Connect4Model.Player.PLAYER1) {
                    rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER1);
                } else {
                    rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER2);
                }
                inARowTwo++;
            }
            r++;
        }
 
        return rating; 
    } 
    
    /**
     * Given the column, row, the board and the player, returns the rating from the col diagonal right 
     * A positive rating is given if the number in a row is mostly p
     * A negative rating is given if the number in a row is mostly not p
     * @param col the column required to be rated
     * @param row the row required to be rated
     * @param board the current state of the board
     * @param p the player to rate by
     * @return returns a positive or negative integer rating of a row depending if it on the player p
     */
    private int diagonalRightRating(int col, int row, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int four = 0;
        int r = row;
        int c = col;
        int inARowOne = 0;
        int inARowTwo = 0;
        
        while (c < NUM_COLS && r < NUM_ROWS && four < 4) {
            Connect4Model.Player check = board.getPlayerAt(r, c);
            if (check == p) { 
                inARowTwo = 0;
                rating = ratingScale(rating, inARowOne, p, check);
                inARowOne++;
            } else if (check == Connect4Model.Player.NO_ONE) {//empty cell
                    if (inARowOne == 0) {
                        rating--;
                    } else if (inARowTwo == 0) {
                        rating++;   
                    }
            } else { // other player D:!
                inARowOne = 0;
                if (check == Connect4Model.Player.PLAYER1) {
                    rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER1);
                } else {
                    rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER2);
                }
                inARowTwo++;
            }
             r++;
             c++;
             four++;
        }
            
        return rating;
    } 
    
    /**
     * Given the column, row, the board and the player, returns the rating from the col diagonal left 
     * A positive rating is given if the number in a row is mostly p
     * A negative rating is given if the number in a row is mostly not p
     * @param col the column required to be rated
     * @param row the row required to be rated
     * @param p the player to rate by
     * @return returns a positive or negative integer rating of a row depending if it on the player
     */
    private int diagonalLeftRating(int col, int row, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int four = 0;
        int r = row;
        int c = col;
        int inARowOne = 0;
        int inARowTwo = 0;
        
        while (c >= 0 && r < NUM_ROWS && four < 4) {
            Connect4Model.Player check = board.getPlayerAt(r, c);
            if (check == p) { 
                inARowTwo = 0;
                rating = ratingScale(rating, inARowOne, p, check);
                inARowOne++;
            } else if (check == Connect4Model.Player.NO_ONE) {//empty cell
                    if (inARowOne == 0) {
                        rating--;
                    } else if (inARowTwo == 0) {
                        rating++;   
                    }
            } else { // other player D:!
                inARowOne = 0;
                if (check == Connect4Model.Player.PLAYER1) {
                    rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER1);
                } else {
                    rating = ratingScale(rating, inARowTwo, p, Connect4Model.Player.PLAYER2);
                }
                inARowTwo++;
            }    
            r++;
            c--;
            four++;
        }
        
        
        return rating;
    } 
   
    /**
     * Determines the rating depending on the number of rows a certain player has
     * If the player given and the current checking player is equal the rating is incremented
     * Otherwise the other player is better off with in a rows and the rating is decremented
     * @param currRating the current rating to increment or decrement on
     * @param inARow the number in a row of the player to check
     * @param given the player that is to be maximized with positive rating
     * @param check the current player to check on with the number of in a rows
     * @return the new rating after changing the current rating
     */
    private int ratingScale(int currRating, int inARow, Connect4Model.Player given, Connect4Model.Player check) {
        int newRating = 0;
        
        if (given == check) {
            if (inARow == 0) {
                newRating = currRating + 0;
            } else if (inARow == 1) {
                newRating = currRating + 1;
            } else if (inARow == 2) {
                newRating = currRating + 10;
            } else if (inARow == 3) {
                newRating = currRating + 100;
            } else {
                newRating = currRating + 1000;
            }
        } else {
            if (inARow == 0) {
                newRating = currRating - 0;
            } else if (inARow == 1) {
                newRating = currRating - 1;
            } else if (inARow == 2) {
                newRating = currRating - 10;
            } else if (inARow == 3) {
                newRating = currRating - 100;
            } else {
                newRating = currRating - 1000;
            }
        }
        
        return newRating;
    }
    
} 

