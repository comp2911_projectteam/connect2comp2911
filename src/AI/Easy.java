package AI;

import model.Connect4Model;
import model.GameBoard;
import java.util.Random;

/**
 * Class that represents an AI with easy difficulty
 * This AI's strategy is to block whenever there is three in a row made by the player.
 * If there is no three in a row to block, it randomly chooses between columns
 * However the AI would not choose a column that would lead to player winning in the next move
 * If AI gets three in a row and the player does not block, then it would win.
 * @author Katrina Mok
 */
public class Easy implements GamePlayer {
    
    private static final int NUM_ROWS = 6;
    private static final int NUM_COLS = 7;
    private static final int INVALID  = -1;
    /**
     * Given the current state of board, it returns the next move made by the AI
     * @param board the current state of the board
     * @return column a valid column number from 0 to 6
     */
    @Override
    public int getNextMove(GameBoard board) {
        int move = checkBestRating(3, board);
        if (move == INVALID) { 
            move = checkBestRating(2, board);
            if (move == INVALID) { // else no other choice (make random good choice)
                int c = 0; 
                boolean check = false;
                while (c <= 6) { // check for any moves that are not going to let player win
                    if (overallColRating(c, board) == 1) {
                        check = true;
                    }
                    c++;
                }
                Random rand = new Random();
                if (check == false) { // no good moves to prevent player winning (pick randomly)
                    do {
                        move = rand.nextInt(NUM_COLS); // picks out of 0 to 6 column numbers
                    } while (board.getInsertLocation(move) == GameBoard.INVALID_INSERT);
                } else { // there are moves that exclude "nextPlayerWins" columns
                    // Only seperate statement to prevent fit looping forever by extra condition
                    do {
                        move = rand.nextInt(NUM_COLS); // picks out of 0 to 6 column numbers
                    } while (board.getInsertLocation(move) == GameBoard.INVALID_INSERT
                            || overallColRating(move, board) == 0);
                }
            }
        }
        
        return move;
    }
    /**
     * Given a rating and the board, searches each column to find if a rating exists
     * @param rating the rating scale of required to search
     * @param board the current state of the board
     * @return column the column with the rating given will be returned. 
     * If the rating required is not found it would return INVALID = -1.
     */
    private int checkBestRating(int rating, GameBoard board) {
        int column = INVALID;
        int c = 0;
        while (c < NUM_COLS) { // check for winning move (computer)
            if (overallColRating(c, board) == rating) {
                column = c;
                break;
            }
            c++;
        }
        return column;
    } 
    
    /**
     * Given a column and the board, it rates the column by preferences, with higher number greater preference
     * @param col the column required to be rated
     * @param board the current state of the board
     * @return the rating of the column in terms of these conditions as follows:
     * If AI has three in a row - the rating of the column is max at 3
     * If Player has three in a row - the rating of the column is then followed by 2
     * If the column allows for player to win if AI places in it the rating is given 0
     * Otherwise the rating is then 1
     */

// returns overall rating of a column choice for AI
    private int overallColRating(int col, GameBoard board) {
        int overall;
        // check if AI has a winning move (3 in a row)
        if (horizontalRating(col, board, Connect4Model.Player.PLAYER2) == 3 ||
                verticalRating(col, board, Connect4Model.Player.PLAYER2) == 3 ||
                diagonalLeftRating(col, board, Connect4Model.Player.PLAYER2) == 3 ||
                diagonalRightRating(col, board, Connect4Model.Player.PLAYER2) == 3) {
            overall = 3; // max rating - right column to win 
        } else if (horizontalRating(col, board, Connect4Model.Player.PLAYER1) == 3 ||
                verticalRating(col, board, Connect4Model.Player.PLAYER1) == 3 ||
                diagonalLeftRating(col, board, Connect4Model.Player.PLAYER1) == 3 ||
                diagonalRightRating(col, board, Connect4Model.Player.PLAYER1) == 3) {
            overall = 2; // next rating after AI win move - need to block the player from winning (3 in a row)
        } else if (nextPlayerWins(col, board)) {
            overall = 0; // player will win if you place in this column
        } else {
            overall = 1; // not as bad as 0
        }

        return overall;
    }
    
    /**
     * Given a column and the board, checks if AI's move results in player winning
     * In other words, checks if AI move in a column results in the player winning in next move with the same column
     * @param col the column required to be rated
     * @param board the current state of the board
     * @return true if the player would win next move in the column given and false otherwise
     */
    private boolean nextPlayerWins(int col, GameBoard board) {
        boolean rating = false;
        int r = board.getInsertLocation(col);
        if (r != GameBoard.INVALID_INSERT) {
            int rowUp = r + 1;
            if (rowUp <= 5) {
                r = rowUp;
                int c = col+1;
                // horizontal check (left and right)
                int bad = 0;
                while (c < NUM_COLS && r < NUM_ROWS) {
                    Connect4Model.Player check = board.getPlayerAt(r, c);
                    if (check == Connect4Model.Player.PLAYER1) {
                        bad++;
                    } else { 
                        break;
                    }
                    c++;
                }
                c = col-1;
                while (c >= 0 && bad < 3 && r < NUM_ROWS) {
                    Connect4Model.Player check = board.getPlayerAt(r, c);
                    if (check == Connect4Model.Player.PLAYER1) {
                        bad++;
                    } else { 
                        break;
                    }
                    c--;
                }
                
                if (bad == 3) {
                    rating = true;
                } else { 
                    bad = 0; // reset check for diagonal
                    // diagonal left check
                    r = board.getInsertLocation(col);
                    c = col+1;
                    // check diagonal downwards left
                    while (c < NUM_COLS && r >= 0) {
                        Connect4Model.Player check = board.getPlayerAt(r, c);
                        if (check == Connect4Model.Player.PLAYER1) { // not good
                            bad++;
                        } else { // blocked by AI
                            break;
                        }
                        r--;
                        c++;
                    }
                    r = board.getInsertLocation(col) + 2;
                    c = col - 1;
                    // check diagonal upwards left
                    while (c >= 0 && r < NUM_ROWS && bad < 3) {
                        Connect4Model.Player check = board.getPlayerAt(r, c);
                        if (check == Connect4Model.Player.PLAYER1) { //not good
                            bad++;
                        } else { // // if it is blocked by other player
                            break;
                        }
                        r++;
                        c--;
                    }
                    if (bad == 3) {
                        rating = true;
                    } else {
                        bad = 0;
                        // diagonal right check   
                        r = board.getInsertLocation(col);
                        c = col-1;
                        // check diagonal downwards right
                        while (c >= 0 && r >= 0) {
                            Connect4Model.Player check = board.getPlayerAt(r, c);
                            if (check == Connect4Model.Player.PLAYER1) { //not good
                                bad++;
                            } else { // // if it is blocked by other player
                                break;
                            }
                            r--;
                            c--;
                        }
                        r = board.getInsertLocation(col)+ 2;
                        c = col+1;
                        // check diagonal upwards right
                        while (c < NUM_COLS && r < NUM_ROWS && bad < 3) {
                            Connect4Model.Player check = board.getPlayerAt(r, c);
                            if (check == Connect4Model.Player.PLAYER1) { //not good
                                bad++;
                            } else { // // if it is blocked by other player
                                break;
                            }
                            r++;
                            c++;
                        }
                        if (bad == 3) {
                            rating = true;
                        }
                    }
                }
            }
                
        } else {
            rating = true;
        }
        
        return rating;
    }
    
    /**
     * Given the column, the board and the player, returns the horizontal rating from the col across 
     * @param col the column required to be rated
     * @param board the current state of the board
     * @param p the player to rate by
     * @return returns a rating between 0 to 3 depending on the number of the player's token in the row across
     */
    private int horizontalRating(int col, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int r = board.getInsertLocation(col);
        if (r != GameBoard.INVALID_INSERT) {
            int c = col+1;
            // check right horizontal
            while (c < NUM_COLS && r < NUM_ROWS) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { // good - there are more connections
                    rating++;
                } else { // if it is blank or blocked by other player
                    break;
                }
                c++;
            }
            c = col-1;
            // check left horizontal
            while (c >= 0 && rating < 3 && r < NUM_ROWS) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { // good - there are more connections
                    rating++;
                } else { // // if it is blank or blocked by other player
                    break;
                }
                c--;
            }
        } 
        return rating; 
    } 
    
    /**
     * Given the column, the board and the player, returns the vertical rating from the col going up 
     * @param col the column required to be rated
     * @param board the current state of the board
     * @param p the player to rate by
     * @return returns a rating between 0 to 3 depending on the number of the player's tokens in a row vertically to col
     */
    private int verticalRating(int col, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int r = board.getInsertLocation(col);
        if (r != GameBoard.INVALID_INSERT && r-1 >= 0) {
            r = r - 1;
            int c = col;
            // check downwards only
            while (r >= 0) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { // good - there are more connections
                    rating++;
                } else { // // if it is blocked by other player
                    break;
                }
                r--;
            }
        } 
        return rating; 
    } 
    
    /**
     * Given the column, the board and the player, returns the rating from the col diagonal left 
     * @param col the column required to be rated
     * @param board the current state of the board
     * @param p the player to rate by
     * @return returns a rating between 0 to 3 depending on the number of the player's tokens in a row diagonally left
     */
    private int diagonalLeftRating(int col, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int row = board.getInsertLocation(col); 
        if (row != GameBoard.INVALID_INSERT) {
            int r = row-1;
            int c = col+1;
            // check diagonal downwards left
            while (c <= 6 && r >= 0) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { // good - there are more connections
                    rating++;
                } else { // // if it is blocked by other player
                    break;
                }
                r--;
                c++;
            }
            r = row + 1;
            c = col - 1;
            // check diagonal upwards left
            while (c >= 0 && r < NUM_ROWS) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { // good - there are more connections
                    rating++;
                } else { // // if it is blocked by other player
                    break;
                }
                r++;
                c--;
            }
        }
        return rating;
    } 
    
     /**
     * Given the column, the board and the player, returns the rating from the col diagonal right 
     * @param col the column required to be rated
     * @param board the current state of the board
     * @param p the player to rate by
     * @return returns a rating between 0 to 3 depending on the number of the player's tokens in a row diagonally right
     */
    private int diagonalRightRating(int col, GameBoard board, Connect4Model.Player p) {
        int rating = 0;
        int row = board.getInsertLocation(col);
        if (row != GameBoard.INVALID_INSERT) {
            int r = row-1;
            int c = col-1;
            // check diagonal downwards right
            while (c >= 0 && r >= 0) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { // good - there are more connections
                    rating++;
                } else { // // if it is blocked by other player
                    break;
                }
                r--;
                c--;
            }
            r = row+1;
            c = col+1;
            // check diagonal upwards right
            while (c < NUM_COLS && r < NUM_ROWS) {
                Connect4Model.Player check = board.getPlayerAt(r, c);
                if (check == p) { // good - there are more connections
                    rating++;
                } else { // // if it is blocked by other player
                    break;
                }
                r++;
                c++;
            }
        }
        return rating;
    } 
}