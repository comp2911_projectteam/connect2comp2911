package AI;

import model.GameBoard;

import java.util.Random;

/**
 * Class that represents an AI with beginner capabilities
 * This AI's strategy is to pick a random valid column to play
 * @author Katrina Mok
 */
public class Beginner implements GamePlayer {
    
    private static final int NUM_COLS = 7;
    /**
     * Given the current state of board, it returns the next move made by the AI
     * @param board the current state of the board
     * @return column a random valid column number from 0 to 6
     */
    @Override
    public int getNextMove(GameBoard board) {
        Random r = new Random();
        int column;
        do {
            column = r.nextInt(NUM_COLS); // picks out of 0 to 6 column numbers
        } while (board.getInsertLocation(column) == GameBoard.INVALID_INSERT);
        
        return column;
    }
    
}
