package AI;

import model.GameBoard;

/**
 * The main interface that all AI classes must implement
 */
public interface GamePlayer {
    /**
     * This is the method that will be called by the game engine to determine the next
     * move for the AI.
     * @param board the current state of the board
     * @return Must return a column number (0-6)
     */
    int getNextMove (GameBoard board);
}
